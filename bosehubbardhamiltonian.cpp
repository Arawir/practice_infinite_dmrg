#include "bosehubbardhamiltonian.h"
#include "basicphisics.h"

BoseHubbardHamiltonianA::BoseHubbardHamiltonianA(OperatorType type, iQBox *box) :
    Operator{type, "BHHA"}
  , Box{box}
{
    arma::mat oI = I(Box->nodeRank());
    arma::mat on = n(Box->nodeRank());
    arma::mat on2 = n2(Box->nodeRank());

    this->mat() = g*on2+T*(on-s*oI)*(on-s*oI);
}

void BoseHubbardHamiltonianA::increase()
{
    arma::mat oHn = this->mat();
    arma::mat oI = I(Box->nodeRank());
    arma::mat oId = I(this->dim());
    arma::mat oa = a(Box->nodeRank());
    arma::mat oat = oa.t();
    arma::mat oakA = Box->observableNamed("akA")->mat();
    arma::mat oakAt = oakA.t();
    arma::mat on = n(Box->nodeRank());
    arma::mat on2 = n2(Box->nodeRank());
    arma::mat oN = Box->observableNamed("NA")->mat();

    this->mat()  = kron(oHn, oI);
    this->mat() += g*kron(oId, on2);
    this->mat() += -J*( kron(oakAt, oa) + kron(oakA, oat) );
    this->mat() += T*( kron(oId, on2) - 2.0*s*kron(oId, on) + 2.0*kron(oN, on) );
}

iOperator *BoseHubbardHamiltonianA::mergedTo(iOperator *Other)
{
    iOperator* Hab = new BoseHubbardHamiltonianA{OperatorType::A, Box};

    arma::mat HaMat = kron( this->mat(), I(Other->dim()) );
    arma::mat HbMat = kron( I(this->dim()), Other->mat() );

    arma::mat SigmaZkaMat = this->Box->observableNamed("akA")->increased()->mat();
    arma::mat SigmaZkbMat = ((BoseHubbardHamiltonianB*)Other)->Box->observableNamed("akB")->increased()->mat();

    arma::mat HabMat = -J*kron( SigmaZkaMat, SigmaZkbMat );
    Hab->mat() = HaMat + HabMat + HbMat;

    return Hab;
}

BoseHubbardHamiltonianA *BoseHubbardHamiltonianA::increased()
{
    BoseHubbardHamiltonianA* OutputHamiltonian = new BoseHubbardHamiltonianA{*this};
    OutputHamiltonian->increase();

    return OutputHamiltonian;
}





BoseHubbardHamiltonianB::BoseHubbardHamiltonianB(OperatorType type, iQBox *box) :
    Operator{type, "BHHA"}
  , Box{box}
{
    arma::mat oI = I(Box->nodeRank());
    arma::mat on = n(Box->nodeRank());
    arma::mat on2 = n2(Box->nodeRank());

    this->mat() = g*on2+T*(on-s*oI)*(on-s*oI); //check this
}

void BoseHubbardHamiltonianB::increase()
{
    arma::mat oHn = this->mat();
    arma::mat oI = I(Box->nodeRank());
    arma::mat oId = I(this->dim());
    arma::mat oa = a(Box->nodeRank());
    arma::mat oat = oa.t();
    arma::mat oakB = Box->observableNamed("akB")->mat();
    arma::mat oakBt = oakB.t();
    arma::mat on = n(Box->nodeRank());
    arma::mat on2 = n2(Box->nodeRank());
    arma::mat oN = Box->observableNamed("NB")->mat();

    this->mat() = kron(oI, oHn)
                + g*kron(on2, oId)
                - J*( kron(oa, oakBt) + kron(oat, oakB) )
                + T*( kron(on2, oId) - 2.0*s*kron(on, oId) + 2.0*kron(on, oN) );
}

BoseHubbardHamiltonianB *BoseHubbardHamiltonianB::increased()
{
    BoseHubbardHamiltonianB* OutputHamiltonian = new BoseHubbardHamiltonianB{*this};
    OutputHamiltonian->increase();

    return OutputHamiltonian;
}

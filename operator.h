#ifndef OPERATOR_H
#define OPERATOR_H

#include <armadillo>
#include <vector>
#include "ioperator.h"


class Operator : public iOperator
{
public:
    Operator(OperatorType type, std::string name);
    virtual ~Operator() = default;

    iOperator &operator = (const iOperator* Other) override;

    arma::mat& mat() override;
    arma::mat mat() const override;
    arma::uword dim() override;

    void rebaseWithTruncation(std::vector<arma::vec> NewEigenVectors) override;

    OperatorType type() const override;
    std::string name() const override;
private:
    OperatorType Type;
    std::string Name;
    arma::mat Matrix;
};


#endif // OPERATOR_H

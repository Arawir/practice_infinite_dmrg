#include "basicphisics.h"
#include "lambda_lanczos/lambda_lanczos.hpp"

double J = 1.0;
double g = 1.0;
double mi = 1.0;
double T = 100.0;
double s = 3.0;

//double J = 1.0;
double h = 1.0;


arma::mat sigmaZ()
{
    arma::Mat<double> SigmaZ(2,2);
    SigmaZ(0,0) = 1.0;
    SigmaZ(0,1) = 0.0;
    SigmaZ(1,0) = 0.0;
    SigmaZ(1,1) = -1.0;
    return SigmaZ;
}

arma::mat sigmaZ2()
{
    return kron(sigmaZ(), sigmaZ());
}

arma::mat sigmaX()
{
    arma::Mat<double> SigmaX(2,2);
    SigmaX(0,0) = 0.0;
    SigmaX(0,1) = 1.0;
    SigmaX(1,0) = 1.0;
    SigmaX(1,1) = 0.0;
    return SigmaX;
}

arma::vec calculateEnergies(arma::mat Hamiltonian, int numberOfLevels)
{
    arma::vec AllEnergies = arma::eig_sym(Hamiltonian);
    arma::vec Out(numberOfLevels);

    sort(AllEnergies);

    for(int i=0; i<numberOfLevels; i++){
        Out[i] = AllEnergies[i];
    }

    return Out;
}

arma::vec calculateEnergies(arma::mat Hamiltonian)
{
    arma::vec AllEnergies = arma::eig_sym(Hamiltonian);
    return AllEnergies;
}

arma::mat I(int dimension)
{
    if(dimension<1){ dimension = 1; }
    return arma::mat{ static_cast<arma::uword>(dimension),
                      static_cast<arma::uword>(dimension),
                      arma::fill::eye};
}

arma::vec calculateBaseState(arma::mat Hamiltonian)
{
    int DimH = sqrt( Hamiltonian.size() );

    auto mv_mul = [&](const std::vector<double>& in, std::vector<double>& out) {
      for(int i=0; i<DimH; i++){
        for(int j=0; j<DimH; j++){
          out[i] += Hamiltonian(i,j)*in[j];
        }
      }
    };

    lambda_lanczos::LambdaLanczos<double> engine(mv_mul, DimH, false);
    double eigenvalue;
    std::vector<double> eigenvector(DimH);
    engine.run(eigenvalue, eigenvector);

    arma::vec BaseStateVector(DimH, arma::fill::zeros);
    for(int i=0; i<DimH; i++){
        BaseStateVector[i] = eigenvector[i];
    }

    return BaseStateVector;
}

arma::vec calculateBaseStateExperimental(arma::mat Hamiltonian, double precission)
{
    int DimH = sqrt( Hamiltonian.size() );
    arma::vec BaseStateVector1(DimH, arma::fill::ones);
    return calculateBaseStateExperimental(Hamiltonian, BaseStateVector1, precission);
}

arma::vec calculateBaseStateExperimental(arma::mat Hamiltonian, arma::vec startingVector, double precission)
{
    double DeltaT = 0.0001;
    int DimH = sqrt( Hamiltonian.size() );

    arma::mat Multiplier = I(DimH)-Hamiltonian*DeltaT;

    arma::vec BaseStateVector1 = startingVector;
    BaseStateVector1 /= arma::norm(BaseStateVector1);

    arma::vec BaseStateVector2 = Multiplier * BaseStateVector1;
    BaseStateVector2 /= arma::norm(BaseStateVector2);


    while(arma::norm(BaseStateVector1-BaseStateVector2) > precission){
        BaseStateVector1 = Multiplier*BaseStateVector2;
        BaseStateVector1 /= arma::norm(BaseStateVector1);
        BaseStateVector2 = Multiplier*BaseStateVector1;
        BaseStateVector2 /= arma::norm(BaseStateVector2);
    }

    return BaseStateVector2;
}

arma::mat operator ^(arma::mat M1, arma::mat M2)
{
    return kron(M1, M2);
}

arma::mat a(int rank)
{
    arma::Mat<double> a(rank, rank, arma::fill::zeros);
    for(int i=0; i<rank-1; i++){
        a(i, i+1) = sqrt(i+1);
    }
    return a;
}

arma::mat n(int rank)
{
    return a(rank).t()*a(rank);
}

arma::mat n2(int rank)
{
    return n(rank)*n(rank);
}

std::vector<arma::vec> calculateEigenvectors(arma::mat Hamiltonian)
{
    arma::mat TmpEigenVec;
    arma::vec TmpEigenVal;
    std::vector<arma::vec> EigenVec;

    arma::eig_sym(TmpEigenVal, TmpEigenVec, Hamiltonian);

    for(unsigned int i=0; i<TmpEigenVal.size(); i++){
        EigenVec.push_back(TmpEigenVec.col(i));
    }

    return EigenVec;
}

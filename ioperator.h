#ifndef IOPERATOR_H
#define IOPERATOR_H

#include <armadillo>


enum class OperatorType
{
    A,
    B,
};

class iOperator
{
public:
    iOperator(){ }
    virtual ~iOperator() = default;

    virtual iOperator &operator = (const iOperator* Other) = 0;

    virtual arma::mat& mat() = 0;
    virtual arma::mat mat() const = 0;
    virtual arma::uword dim() = 0;

    virtual iOperator* increased() = 0;
    virtual iOperator* mergedTo(iOperator *Other) = 0;
    virtual void increase() = 0;
    virtual void rebaseWithTruncation(std::vector<arma::vec> NewEigenVectors) = 0;

    virtual OperatorType type() const = 0;
    virtual std::string name() const = 0;
};


#endif // IOPERATOR_H

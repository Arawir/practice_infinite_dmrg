#include "sigmazsum.h"
#include "basicphisics.h"


SigmaZsumA::SigmaZsumA(OperatorType type) :
    Operator{OperatorType::A, "SigmaZsumA"}
{
    this->mat() = sigmaZ();
}


void SigmaZsumA::increase()
{
    this->mat() = kron(this->mat(),I(2)) + kron(I(this->dim()), sigmaZ());
}

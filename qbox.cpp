#include "qbox.h"


QBox::QBox() :
    NumberOfNodes{1}
  , BSHS{ } //????
{

}

void QBox::setHamiltonianA(iOperator *hamiltonianA)
{
    HamiltonianA = hamiltonianA;
}

void QBox::setHamiltonianB(iOperator *hamiltonianB)
{
   HamiltonianB = hamiltonianB;
}

void QBox::addObservable(iOperator *newObservable)
{
    Observables.push_back(newObservable);
}

void QBox::addNode()
{
    HamiltonianA->increase();
    HamiltonianB->increase();
    for(iOperator* observable : Observables){
        observable->increase();
    }

    NumberOfNodes++;
}



void QBox::addNodes(int nodesToAdd)
{
    for(int i=0; i<nodesToAdd; i++){
        addNode();
    }
}

void QBox::addNodeWithDMRG()
{
    infiniteDMRG();
    addNode();
    rebaseOperators();
}

void QBox::addNodesWithDMRG(int nodesToAdd)
{
    for(int i=0; i<nodesToAdd; i++){
        addNodeWithDMRG();
    }
}

void QBox::showData()
{
    output << "Number of nodes: " << NumberOfNodes << std::endl
          // << "Energies: " << calculateEnergies(HamiltonianA->mat()) << std::endl
           << "Number of particles A: " << averrageValueOfObservable("NA")
           << std::endl;
}

iOperator *QBox::observableNamed(std::string name)
{
    for(iOperator* observable : Observables){
        if(observable->name() == name){ return observable; }
    }
    return nullptr;
}

int QBox::nodeRank()
{
    return NodeRank;
}

int QBox::numberOfNodes()
{
    return NumberOfNodes;
}

double QBox::averrageValueOfObservable(std::string name)
{
    double Value=0.0;
    arma::vec vector = calculateBaseState(HamiltonianA->mat());

    Value = as_scalar(vector.t() * observableNamed(name)->mat() * vector);

    return Value;
}

void QBox::rebaseOperators()
{
    HamiltonianA->rebaseWithTruncation(BVA);
    HamiltonianB->rebaseWithTruncation(BVB);
    for(iOperator* observable : Observables){
        if(observable->type() == OperatorType::A){
            observable->rebaseWithTruncation(BVA);
        } else {
            observable->rebaseWithTruncation(BVB);
        }
    }
}

void QBox::infiniteDMRG()
{
    if(BSHS.empty()){
        BSHS = arma::vec(HamiltonianA->dim()*HamiltonianB->dim()*NodeRank*NodeRank, arma::fill::ones);
    }
    auto StepData = infiniteDmrgStep(HamiltonianA, HamiltonianB, BSHS);
    BVA = std::get<0>(StepData);
    BVB = std::get<1>(StepData);
    BSHS = std::get<2>(StepData);
}


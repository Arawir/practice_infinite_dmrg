#include "userinterface.h"
#include "dmrg.h"
#include "isinghamiltonian.h"
#include "sigmaz.h"

#include "bosehubbardhamiltonian.h"
#include "aka.h"

#include "qbox.h"

int main()
{    
    QBox System;

//    System.setHamiltonianA( new IsingHamiltonianA{OperatorType::A, &System} );
//    System.setHamiltonianB( new IsingHamiltonianB{OperatorType::B, &System} );
//    System.addObservable( new SigmaZkA{OperatorType::A});
//    System.addObservable( new SigmaZkB{OperatorType::B});

    System.setHamiltonianA( new BoseHubbardHamiltonianA{OperatorType::A, &System} );
    System.setHamiltonianB( new BoseHubbardHamiltonianB{OperatorType::B, &System} );
    System.addObservable( new AkA{System.nodeRank()});
    System.addObservable( new AkB{System.nodeRank()});
    System.addObservable( new NA{System.nodeRank()});
    System.addObservable( new NB{System.nodeRank()});

    System.showData();
   // output << System.observableNamed("NA")->mat() << std::endl;

    for(int i=1; i<50; i++){
        System.addNodesWithDMRG(1);
      //  output << System.observableNamed("NA")->mat() << std::endl;
        System.showData();
    }




    return 0;
}

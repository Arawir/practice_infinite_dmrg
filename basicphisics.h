#ifndef BASICPHISICS
#define BASICPHISICS

#include <armadillo>
#include <iostream>
#include <iomanip>
#include <vector>

#define uint unsigned int
#define output std::cout << std::fixed << std::setprecision(4)
#define output10 std::cout << std::fixed << std::setprecision(10)

extern double J;
extern double g;
extern double mi;
extern double T;
extern double s;


extern double J;
extern double h;


arma::mat I(int dimension);

arma::mat sigmaZ();
arma::mat sigmaZ2();

arma::mat sigmaX();

arma::mat a(int rank);
arma::mat n(int rank);
arma::mat n2(int rank);

arma::vec calculateEnergies(arma::mat Hamiltonian, int numberOfLevels);
arma::vec calculateEnergies(arma::mat Hamiltonian);

std::vector<arma::vec> calculateEigenvectors(arma::mat Hamiltonian);

arma::vec calculateBaseState(arma::mat Hamiltonian);
arma::vec calculateBaseStateExperimental(arma::mat Hamiltonian, double precission);
arma::vec calculateBaseStateExperimental(arma::mat Hamiltonian, arma::vec startingVector, double precission);

arma::mat operator ^(arma::mat M1, arma::mat M2);

#endif // BASICPHISICS

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -std=c++11 -larmadillo -llapack -lblas

SOURCES += main.cpp \
    basicphisics.cpp \
    dmrg.cpp \
    userinterface.cpp \
    operator.cpp \
    isinghamiltonian.cpp \
    sigmazsum.cpp \
    qbox.cpp \
    sigmaz.cpp \
    bosehubbardhamiltonian.cpp \
    aka.cpp \
    particlesnumber.cpp

HEADERS += \
    basicphisics.h \
    dmrg.h \
    userinterface.h \
    operator.h \
    isinghamiltonian.h \
    sigmazsum.h \
    qbox.h \
    ioperator.h \
    sigmaz.h \
    iqbox.h \
    bosehubbardhamiltonian.h \
    aka.h \
    particlesnumber.h

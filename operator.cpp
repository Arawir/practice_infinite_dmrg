#include "operator.h"
#include "basicphisics.h"


Operator::Operator(OperatorType type, std::string name) :
    Type{type}
  , Name{name}
  , Matrix{}
{

}

iOperator &Operator::operator =(const iOperator* Other)
{
    Type = Other->type();
    Name = Other->name();
    Matrix = Other->mat();
    return *this;
}

arma::mat &Operator::mat()
{
    return Matrix;
}

arma::mat Operator::mat() const
{
    return Matrix;
}

arma::uword Operator::dim()
{
    return sqrt( Matrix.size() );
}

void Operator::rebaseWithTruncation(std::vector<arma::vec> NewEigenVectors)
{
    arma::uword NewDim = NewEigenVectors.size();
    arma::mat NewMat(NewDim, NewDim, arma::fill::zeros);

    for(arma::uword i=0; i<NewDim; i++){
        for(arma::uword j=0; j<NewDim; j++){
            NewMat(i,j) = as_scalar(NewEigenVectors[i].t() * Matrix * NewEigenVectors[j]);
        }
    }
    Matrix = NewMat;
}

OperatorType Operator::type() const
{
    return Type;
}

std::string Operator::name() const
{
    return Name;
}

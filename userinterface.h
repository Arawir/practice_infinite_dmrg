#ifndef USERINTERFACE
#define USERINTERFACE

#include "basicphisics.h"
#include "operator.h"

struct ResponseConfig{
    bool ShowSteps = true;
    bool ShowHSEigenvalues = false;
    bool ShowDMa = false;
    bool ShowDMb = false;
    bool ShowBVa = false;
    bool ShowBVb = false;
    bool ShowHaAtTheBeginningOfIteration = false;
    bool ShowHbAtTheBeginningOfIteration = false;
    bool ShowHaAtTheEndOfIteration = false;
    bool ShowHbAtTheEndOfIteration = false;
    bool ShowSigmaZkaAtTheBeginningOfIteration = false;
    bool ShowSigmaZkbAtTheBeginningOfIteration = false;
    bool ShowSigmaZkaAtTheEndOfIteration = false;
    bool ShowSigmaZkbAtTheEndOfIteration = false;
    bool ShowTimings = false;

    bool ShowTheoreticalEnergies = true;
};

int getBasicDimension();
int getNumberOfNodes(int basicDimension);
ResponseConfig selectResponseConfig();

void showOutputData(iOperator* Ha, std::vector<iOperator*> Observables, int NumberOfNodes, int BasicDimension, ResponseConfig Config);


#endif // USERINTERFACE

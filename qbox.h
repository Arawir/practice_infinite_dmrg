#ifndef QBOX_H
#define QBOX_H

#include "userinterface.h"
#include "dmrg.h"
#include "isinghamiltonian.h"
#include "sigmazsum.h"

#include "iqbox.h"

class QBox : public iQBox
{
public:
    QBox();
    ~QBox() = default;

    void setHamiltonianA(iOperator* hamiltonianA) override;
    void setHamiltonianB(iOperator* hamiltonianB) override;
    void addObservable(iOperator* newObservable) override;

    void addNodes(int nodesToAdd) override;
    void addNodesWithDMRG(int nodesToAdd) override;
    void showData() override;

    iOperator* observableNamed(std::string name) override;
    int nodeRank() override;
    int numberOfNodes() override;

    double averrageValueOfObservable(std::string name);

private:
    void addNode();
    void addNodeWithDMRG();
    void rebaseOperators();
    void infiniteDMRG();

private:
    int NodeRank = 4;
    int NumberOfNodes;
    iOperator* HamiltonianA;
    iOperator* HamiltonianB;
    std::vector<iOperator*> Observables;

    std::vector<arma::vec> BVA, BVB;
    arma::vec BSHS;
};

#endif // QBOX_H

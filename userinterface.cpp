#include "userinterface.h"

#include "isinghamiltonian.h"

int getBasicDimension()
{
    int BasicDimension=0;

    output << "Enter basic dimension : ";
    std::cin >> BasicDimension;

    while(BasicDimension<1){
        output << "Basic dimension must be higher or equal 1!" << std::endl
               << "Enter basic dimension : ";
        std::cin >> BasicDimension;
    }
    return BasicDimension;
}

int getNumberOfNodes(int basicDimension){
    int NumberOfNodes=0;

    output << "Enter the number of nodes : ";
    std::cin >> NumberOfNodes;

    while(NumberOfNodes<basicDimension){
        output << "Number of nodes must be higher or equal to basic dimension (" << basicDimension << ")!" << std::endl
               << "Enter the number of nodes : ";
        std::cin >> NumberOfNodes;
    }
    return NumberOfNodes;
}

ResponseConfig selectResponseConfig(){
    ResponseConfig Config;

    output << "Response config : show He Eigenvalues? (0/1) : ";
    std::cin >> Config.ShowHSEigenvalues;
    output << "Response config : show DMa? (0/1) : ";
    std::cin >> Config.ShowDMa;
    output << "Response config : show DMb? (0/1) : ";
    std::cin >> Config.ShowDMb;
    output << "Response config : show BVa? (0/1) : ";
    std::cin >> Config.ShowBVa;
    output << "Response config : show BVb? (0/1) : ";
    std::cin >> Config.ShowBVb;
    output << "Response config : show Ha at the beginning of iteration? (0/1) : ";
    std::cin >> Config.ShowHaAtTheBeginningOfIteration;
    output << "Response config : show Hb at the beginning of iteration? (0/1) : ";
    std::cin >> Config.ShowHbAtTheBeginningOfIteration;
    output << "Response config : show Ha at the end of iteration? (0/1) : ";
    std::cin >> Config.ShowHaAtTheEndOfIteration;
    output << "Response config : show Hb at the end of iteration? (0/1) : ";
    std::cin >> Config.ShowHbAtTheEndOfIteration;
    output << "Response config : show SigmaZka at the beginning of iteration? (0/1) : ";
    std::cin >> Config.ShowSigmaZkaAtTheBeginningOfIteration;
    output << "Response config : show SigmaZkb at the beginning of iteration? (0/1) : ";
    std::cin >> Config.ShowSigmaZkbAtTheBeginningOfIteration;
    output << "Response config : show SigmaZka at the end of iteration? (0/1) : ";
    std::cin >> Config.ShowSigmaZkaAtTheEndOfIteration;
    output << "Response config : show SigmaZkb at the end of iteration? (0/1) : ";
    std::cin >> Config.ShowSigmaZkbAtTheEndOfIteration;

    return Config;
}


void showOutputData(iOperator* Ha, std::vector<iOperator*> Observables, int NumberOfNodes, int BasicDimension, ResponseConfig Config)
{
    output << NumberOfNodes
           << " nodes system with one node dimension " << BasicDimension
           << std::endl;

    output << "Energies (DMRG):" << std::endl
           << calculateEnergies(Ha->mat())
           << std::endl;



    if(Config.ShowTheoreticalEnergies){
//        IsingHamiltonianA Tmp;
//        for(int i=0; i<NumberOfNodes-1; i++){
//            Tmp.increase();
//        }
//        output << "Lowest energies (brute-force diagonalization):" << std::endl
//               << calculateEnergies(Tmp, pow(2,BasicDimension))
//               << std::endl;
    }

    //arma::vec BSt = calculateBaseState(Tmp);
    arma::vec BSa = calculateBaseState(Ha->mat());
    output << "Averrage values of observables: " << std::endl;
    for(iOperator* observable : Observables){
        output << as_scalar( BSa.t() * observable->mat() * BSa ) / NumberOfNodes << std::endl;
    }

}

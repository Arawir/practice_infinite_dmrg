#ifndef BOSEHUBBARDHAMILTONIAN_H
#define BOSEHUBBARDHAMILTONIAN_H

#include "operator.h"
#include "iqbox.h"

class BoseHubbardHamiltonianA;
class BoseHubbardHamiltonianB;


class BoseHubbardHamiltonianA : public Operator
{
public:
    BoseHubbardHamiltonianA(OperatorType type, iQBox* box);
    ~BoseHubbardHamiltonianA() = default;

    void increase() override;
    iOperator* mergedTo(iOperator *Other) override; //ugly, fix this!
    BoseHubbardHamiltonianA* increased() override;

public:
    iQBox* Box;
};


class BoseHubbardHamiltonianB : public Operator
{
public:
    BoseHubbardHamiltonianB(OperatorType type, iQBox* box);
    ~BoseHubbardHamiltonianB() = default;

    void increase() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    BoseHubbardHamiltonianB* increased() override;

public:
    iQBox* Box;
};

#endif // BOSEHUBBARDHAMILTONIAN_H

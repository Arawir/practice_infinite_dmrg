#include "sigmaz.h"
#include "basicphisics.h"


SigmaZkA::SigmaZkA(OperatorType type) :
    Operator{type, "SigmaZkA"}
{
    this->mat() = sigmaZ();
}

SigmaZkA::SigmaZkA(const SigmaZkA &Other) :
    Operator{OperatorType::A, "SigmaZkA"}
{
    this->mat() = Other.mat();
}

iOperator *SigmaZkA::increased()
{
    SigmaZkA* Increased = new SigmaZkA{*this};
    Increased->increase();

    return Increased;
}

void SigmaZkA::increase()
{
    this->mat() = kron( I(this->dim()), sigmaZ());
}


SigmaZkB::SigmaZkB(OperatorType type) :
    Operator{type, "SigmaZkB"}
{
    this->mat() = sigmaZ();
}

SigmaZkB::SigmaZkB(const SigmaZkB &Other) :
    Operator{OperatorType::B, "SigmaZkB"}
{
    this->mat() = Other.mat();
}

iOperator *SigmaZkB::increased()
{
    SigmaZkB* Increased = new SigmaZkB{*this};
    Increased->increase();

    return Increased;
}

void SigmaZkB::increase()
{
    this->mat() = kron( sigmaZ(), I(this->dim()) );
}

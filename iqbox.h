#ifndef IQBOX_H
#define IQBOX_H

#include "ioperator.h"

class iQBox
{
public:
    iQBox(){ }
    virtual ~iQBox() = default;
    virtual void setHamiltonianA(iOperator* hamiltonianA) = 0;
    virtual void setHamiltonianB(iOperator* hamiltonianB) = 0;
    virtual void addObservable(iOperator* newObservable) = 0;
    virtual void addNodes(int nodesToAdd) = 0;
    virtual void addNodesWithDMRG(int nodesToAdd) = 0;
    virtual void showData() = 0;
    virtual iOperator* observableNamed(std::string name) = 0;
    virtual int nodeRank() = 0;
    virtual int numberOfNodes() = 0;
};

#endif // IQBOX_H

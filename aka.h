#ifndef AKA_H
#define AKA_H

#include "operator.h"

class AkA : public Operator
{
public:
    AkA(int size);
    ~AkA() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
private:
    int OneNodeSize;
};


class AkB : public Operator
{
public:
    AkB(int size);
    ~AkB() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
private:
    int OneNodeSize;
};


class NA : public Operator
{
public:
    NA(int size);
    ~NA() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
private:
    int OneNodeSize;
};


class NB : public Operator
{
public:
    NB(int size);
    ~NB() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
private:
    int OneNodeSize;
};

#endif // AKA_H

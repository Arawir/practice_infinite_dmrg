#ifndef SIGMAZSUM_H
#define SIGMAZSUM_H

#include "operator.h"


class SigmaZsumA : public Operator
{
public:
    SigmaZsumA(OperatorType type);
    ~SigmaZsumA() = default;

    iOperator* increased() override{ return nullptr; };
    iOperator* mergedTo(iOperator *Other) override{ return Other; };
    void increase() override;
};

#endif // SIGMAZSUM_H

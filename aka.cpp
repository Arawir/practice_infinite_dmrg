#include "aka.h"

#include "basicphisics.h"

AkA::AkA(int size) :
    Operator{ OperatorType::A, "akA"}
  , OneNodeSize{size}
{
    this->mat() = a(OneNodeSize);
}

void AkA::increase()
{
    this->mat() = kron( I(this->dim()), a(OneNodeSize));
}

iOperator *AkA::increased()
{
    AkA* Increased = new AkA{*this};
    Increased->increase();

    return Increased;
}



AkB::AkB(int size) :
    Operator{ OperatorType::B, "akB"}
  , OneNodeSize{size}
{
    this->mat() = a(OneNodeSize);
}

iOperator *AkB::increased()
{
    AkB* Increased = new AkB{*this};
    Increased->increase();

    return Increased;
}

void AkB::increase()
{
    this->mat() = kron(a(OneNodeSize), I(this->dim()));
}



NA::NA(int size) :
    Operator{ OperatorType::A, "NA"}
  , OneNodeSize{size}
{
    this->mat() = n(OneNodeSize);
}

iOperator *NA::increased()
{
    NA* Increased = new NA{*this};
    Increased->increase();

    return Increased;
}

void NA::increase()
{
    this->mat() = kron(this->mat(), I(OneNodeSize))
                + kron(I(this->dim()), n(OneNodeSize));
}



NB::NB(int size) :
    Operator{ OperatorType::B, "NB"}
  , OneNodeSize{size}
{
    this->mat() = n(OneNodeSize);

}

iOperator *NB::increased()
{
    NB* Increased = new NB{*this};
    Increased->increase();

    return Increased;
}

void NB::increase()
{
    this->mat() = kron( I(OneNodeSize), this->mat() )
                + kron( n(OneNodeSize), I(this->dim()) );
}

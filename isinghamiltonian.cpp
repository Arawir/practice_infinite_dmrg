#include "isinghamiltonian.h"
#include "basicphisics.h"

IsingHamiltonianA::IsingHamiltonianA(OperatorType type, iQBox* box) :
    Operator{type, "HA"}
  , Box{box}
{
    this->mat() = -h*sigmaX(); //one node hamiltonian
}

IsingHamiltonianA::IsingHamiltonianA(const IsingHamiltonianA &Other) :
    Operator{OperatorType::A, "HA"}
  , Box{Other.Box}
{
    this->mat() = Other.mat();
}

void IsingHamiltonianA::increase()
{
    this->mat() = kron(this->mat(), I(2)) + kron(I(this->dim()),(-h*sigmaX())) + kron(Box->observableNamed("SigmaZkA")->mat(),(-J*sigmaZ()));
}

IsingHamiltonianA* IsingHamiltonianA::increased()
{
    IsingHamiltonianA* OutputHamiltonian = new IsingHamiltonianA{*this};
    OutputHamiltonian->increase();

    return OutputHamiltonian;
}




iOperator *IsingHamiltonianA::mergedTo(iOperator *Other)
{
    iOperator* Hab = new IsingHamiltonianA{OperatorType::A, Box};

    arma::mat HaMat = kron( this->mat(), I(Other->dim()) );
    arma::mat HbMat = kron( I(this->dim()), Other->mat() );

    arma::mat SigmaZkaMat = this->Box->observableNamed("SigmaZkA")->increased()->mat();
    arma::mat SigmaZkbMat = ((IsingHamiltonianB*)Other)->Box->observableNamed("SigmaZkB")->increased()->mat();

    arma::mat HabMat = kron( SigmaZkaMat, SigmaZkbMat );
    Hab->mat() = HaMat + HabMat + HbMat;


    return Hab;
}




IsingHamiltonianB::IsingHamiltonianB(OperatorType type, iQBox* box) :
    Operator{type, "HB"}
  , Box{box}
{
    this->mat() = -h*sigmaX(); //one node hamiltonian
}

IsingHamiltonianB::IsingHamiltonianB(const IsingHamiltonianB &Other) :
    Operator{OperatorType::B, "HB"}
  , Box{Other.Box}
{
    this->mat() = Other.mat();
}

void IsingHamiltonianB::increase()
{
    this->mat() = kron(I(2), this->mat()) + kron(-h*sigmaX(), I(this->dim()))+ kron(-J*sigmaZ(), Box->observableNamed("SigmaZkB")->mat());
}

IsingHamiltonianB* IsingHamiltonianB::increased()
{
    IsingHamiltonianB* OutputHamiltonian = new IsingHamiltonianB{*this};
    OutputHamiltonian->increase();

    return OutputHamiltonian;
}

iOperator *IsingHamiltonianB::mergedTo(iOperator *Other)
{
    IsingHamiltonianB* Hab = new IsingHamiltonianB{OperatorType::B, Box};

    arma::mat HaMat = kron( Other->mat(), I(this->dim()) );
    arma::mat HbMat = kron( I(Other->dim()), this->mat() );

    arma::mat SigmaZkaMat = ((IsingHamiltonianA*)Other)->Box->observableNamed("SigmaZkA")->increased()->mat();
    arma::mat SigmaZkbMat = this->Box->observableNamed("SigmaZkB")->increased()->mat();

    arma::mat HabMat = kron( SigmaZkaMat, SigmaZkbMat );

    Hab->mat() = HaMat + HabMat + HbMat;

    return Hab;
}




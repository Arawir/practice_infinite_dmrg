#ifndef SIGMAZ_H
#define SIGMAZ_H

#include "operator.h"

class SigmaZkA : public Operator
{
public:
    SigmaZkA(OperatorType type);
    SigmaZkA(const SigmaZkA &Other);
    ~SigmaZkA() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
};




class SigmaZkB : public Operator
{
public:
    SigmaZkB(OperatorType type);
    SigmaZkB(const SigmaZkB &Other);
    ~SigmaZkB() = default;

    iOperator* increased() override;
    iOperator* mergedTo(iOperator *Other) override{ return Other; }
    void increase() override;
};

#endif // SIGMAZ_H

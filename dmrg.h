#ifndef DMRG
#define DMRG


#include <vector>

#include "ioperator.h"

arma::mat prepareSuperblockHamiltonian(iOperator *Ha, iOperator *Hb);
arma::mat prepareBasicStateDensityMatrix(arma::vec BaseStateVector);
arma::mat partialTraceA(arma::mat DMS);
arma::mat partialTraceB(arma::mat DMS);
std::vector<arma::vec> prepareEigenvectors(arma::mat DMA, int numberOfVectors);

void prepareBasicData(iOperator *Ha, iOperator *Hb, std::vector<iOperator*> Observables, int BasicDimension);
std::tuple< std::vector<arma::vec>, std::vector<arma::vec>, arma::vec > infiniteDmrgStep(iOperator *Ha, iOperator *Hb, arma::vec BSHs);
void infiniteDMRG(iOperator *Ha, iOperator *Hb, std::vector<iOperator*> Observables, int NodesToAdd);

#endif // DMRG

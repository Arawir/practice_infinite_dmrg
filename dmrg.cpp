#include "dmrg.h"
#include "basicphisics.h"
#include "isinghamiltonian.h"


arma::mat prepareSuperblockHamiltonian(iOperator *Ha, iOperator *Hb)
{
    iOperator* HaIncreased = Ha->increased();
    iOperator* HbIncreased = Hb->increased();
    arma::mat Hs = HaIncreased->mergedTo(HbIncreased)->mat();

   // output << calculateEnergies(Hs) << std::endl;
    delete HaIncreased;
    delete HbIncreased;
    return Hs;
}

arma::mat prepareBasicStateDensityMatrix(arma::vec BaseStateVector)
{
    return BaseStateVector*BaseStateVector.t();
}

arma::mat partialTraceA(arma::mat DMS)
{
    arma::uword DMBSize = static_cast<int>(pow(DMS.size(), 0.25));
    arma::mat DMB{ DMBSize, DMBSize, arma::fill::zeros };

    for(arma::uword k=0; k<DMBSize*DMBSize; k+=DMBSize){
        for(arma::uword i=0; i<DMBSize; i++){
            for(arma::uword j=0; j<DMBSize; j++){
                DMB(i,j) += DMS(k+i,k+j);
            }
        }
    }

    return DMB;
}

arma::mat partialTraceB(arma::mat DMS)
{
    arma::uword DMASize = static_cast<int>(pow(DMS.size(), 0.25));
    arma::mat DMA{ DMASize, DMASize, arma::fill::zeros};

    for(arma::uword i=0; i<DMASize*DMASize; i+=DMASize){
        for(arma::uword j=0; j<DMASize*DMASize; j+=DMASize){
            for(arma::uword k=0; k<DMASize; k++){
                DMA(i/DMASize,j/DMASize) += DMS(k+i,k+j);
            }
        }
    }

    return DMA;
}

std::vector<arma::vec> prepareEigenvectors(arma::mat DMA, int numberOfVectors)
{
    arma::vec TmpEigenVal;
    arma::mat TmpEigenVec;
    arma::eig_sym(TmpEigenVal, TmpEigenVec, DMA);

    std::vector<arma::vec> EigenVec;
    std::vector<double> EigenVal;
    std::vector<arma::vec> Out;

    for(unsigned int i=0; i<TmpEigenVal.size(); i++){
        EigenVal.push_back(TmpEigenVal[i]);
        EigenVec.push_back(TmpEigenVec.col(i));
    }

    for(unsigned int i=0; i<TmpEigenVal.size(); i++){
        for(unsigned int j=0; j<TmpEigenVal.size()-1; j++){
            if(EigenVal[j]<EigenVal[j+1]){
                std::swap(EigenVal[j], EigenVal[j+1]);
                std::swap(EigenVec[j], EigenVec[j+1]);
            }
        }
    }

    for(int i=0; i<numberOfVectors; i++){
        Out.push_back(EigenVec[i]);
    }

    return Out;
}





void prepareBasicData(iOperator *Ha, iOperator *Hb, std::vector<iOperator*> Observables, int BasicDimension)
{
    for(int i=1; i<BasicDimension; i++){
        Ha->increase();
        Hb->increase();
        for(iOperator* observable : Observables){
            observable->increase();
        }
    }
}

std::tuple< std::vector<arma::vec>, std::vector<arma::vec>, arma::vec > infiniteDmrgStep(iOperator *Ha, iOperator *Hb, arma::vec BSHs)
{
    arma::mat Hs = prepareSuperblockHamiltonian(Ha, Hb);
   // arma::vec NewBSHs = calculateBaseStateExperimental(Hs, 0.0001);
    arma::vec NewBSHs = calculateBaseStateExperimental(Hs, BSHs, 0.0001);
    //arma::vec NewBSHs = calculateBaseState(Hs); //check this!!!
    arma::mat DMs = prepareBasicStateDensityMatrix(NewBSHs);
    arma::mat DMa = partialTraceB(DMs);
    arma::mat DMb = partialTraceA(DMs);
    std::vector<arma::vec> BVa = prepareEigenvectors(DMa, Ha->dim());
    std::vector<arma::vec> BVb = prepareEigenvectors(DMb, Hb->dim());
    return std::make_tuple(BVa, BVb, NewBSHs);
}

void infiniteDMRG(iOperator *Ha, iOperator *Hb, std::vector<iOperator*> Observables, int NodesToAdd)
{
    arma::vec BSHs(Ha->dim()*Hb->dim()*4, arma::fill::ones);
    std::vector<arma::vec> BVa, BVb;

    for(int i=0; i<NodesToAdd; i++){
        output << "Step: " << i+1 << std::endl;
        auto StepData = infiniteDmrgStep(Ha, Hb, BSHs);
        BVa = std::get<0>(StepData);
        BVb = std::get<1>(StepData);
        BSHs = std::get<2>(StepData);

        Ha->increase();
        Ha->rebaseWithTruncation(BVa);
        Hb->increase();
        Hb->rebaseWithTruncation(BVb);
       // output << Ha->mat() << std::endl;
        for(iOperator* observable : Observables){
            observable->increase();
            if(observable->type() == OperatorType::A){
                observable->rebaseWithTruncation(BVa);
            } else {
                observable->rebaseWithTruncation(BVb);
            }
        }
        //output << Observables[1]->mat() << std::endl;
    }
}

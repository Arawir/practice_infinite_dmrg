#ifndef ISINGHAMILTONIAN_H
#define ISINGHAMILTONIAN_H

#include "operator.h"
#include "iqbox.h"

class IsingHamiltonianA;
class IsingHamiltonianB;

class IsingHamiltonianA : public Operator
{
public:
    IsingHamiltonianA(OperatorType type, iQBox* box);
    IsingHamiltonianA(const IsingHamiltonianA &Other);
    ~IsingHamiltonianA() = default;

    void increase() override;
    iOperator* mergedTo(iOperator *Other) override; //ugly, fix this!
    IsingHamiltonianA* increased() override;

public:
    iQBox* Box;
};



class IsingHamiltonianB : public Operator
{
public:
    IsingHamiltonianB(OperatorType type, iQBox* box);
    IsingHamiltonianB(const IsingHamiltonianB &Other);
    ~IsingHamiltonianB() = default;

    void increase() override;

    iOperator* mergedTo(iOperator *Other) override; //ugly, fix this!
    IsingHamiltonianB* increased() override;

public:
    iQBox* Box;
};

#endif // ISINGHAMILTONIAN_H
